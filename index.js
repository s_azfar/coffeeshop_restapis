
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const express = require('express');
var cors = require('cors');
const app = express();
const logger1 = require('morgan');
const passport = require('passport');
const bodyParser = require('body-parser');
const PORT = parseInt(process.env.PORT, 10) || 4200;
const logger = require('./middleware/logger')
const http = require('http');

app.use(logger);
app.use(cors());
app.use(logger1('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

require('./router/index')(app);


const server = http.createServer(app);
server.listen(PORT);

module.exports = app;
