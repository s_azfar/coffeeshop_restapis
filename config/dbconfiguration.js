"use strict";

// const path = require('path');
require('dotenv').config();

module.exports = {
    // root: path.resolve(__dirname + '/../'),
    // port: process.env.PORT,
    DB_NAME: process.env.DB_NAME,
    DB_URI: process.env.HOST,
    DB_USER: process.env.DB_USER,
    DB_PASS: process.env.DB_PASS,
    DB_DIALECT: process.env.DB_DIALECT,
    // logMaxFileSize: process.env.LOGMAXFILESIZE,
    // logMaxFiles: process.env.LOGMAXFILES,
    // SECRET: process.env.SECRET,

};
