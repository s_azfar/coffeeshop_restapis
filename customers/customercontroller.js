const models = require('../models/index');
const db = require('../db/dbcore');
const helper = require('../utils/helper');

exports.getCustomers = async function (req, res) {

    let query = {};
    query.attributes = ['id', 'first_name', 'last_name', 'email', 'contact', 'status', 'image'];
//    query.include = [{model:models.coffee, as:'coffees', through: {attributes: ["total","quantity"]}}];

    let result = await db.get('customer', query, false);

    (result !== null) ?
        res.json(result) :
        res.json({status: "No Data available."});

};

exports.getCustomer = async function (req, res) {

    let query = {};
    query.attributes = ['id', 'first_name', 'last_name', 'email', 'contact', 'status', 'image'];
    // query.include = [{model:models.coffee, as:'coffees'}];
    query.where = {id: req.params.id};

    let result = await db.get('customer', query, true);

    (result !== null) ?
        res.json(result) :
        res.json({status: "No Data available"});
}

exports.saveCustomer = async function (req, res) {
    let query = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        contact: req.body.contact,
        status: req.body.status,
        image: ""
    };

    // return res.json(req.body);

    let result = await db.save('customer', query);

    (result !== null) ?
        res.json(result) :
        res.json({status: "No Data available"});

}

exports.updateCustomer = async function (req, res) {
    let query = {}
    query.attributes = ['id', 'first_name', 'last_name', 'email', 'contact', 'status', 'image'];
    // query.include = [{model:models.category, as:'category'}];
    query.where = {id: req.params.id};

    let newItem = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        contact: req.body.contact,
        status: req.body.status,
        image: req.body.image
    }

    let result = await db.update('customer', query, newItem);

    (result !== null) ?
        res.json(result) :
        res.json({status: "No Data available"});
}

exports.deleteCustomer = async (req, res) => {
    let query = {};
    query.attributes = ['id', 'first_name', 'last_name', 'email', 'contact', 'status', 'image'];
    query.where = {id: req.params.id};

    let result = await db.delete('customer', query);

    (result != null) ?
        res.json(result) :
        res.json({status: "Item not deleted"});
}
