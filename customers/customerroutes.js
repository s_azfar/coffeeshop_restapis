const customercontroller = require ('./customercontroller');

module.exports=(app)=>{

app.get("/api/customers", customercontroller.getCustomers);
app.get("/api/customers/:id",customercontroller.getCustomer);
app.post("/api/customers", customercontroller.saveCustomer);    
app.put("/api/customers/:id", customercontroller.updateCustomer);
app.delete("/api/customers/:id",customercontroller.deleteCustomer);
}
