const multer = require('multer');
const path = require('path');


exports.isRealValue=(obj)=> {
    return obj && obj !== 'null' && obj !== 'undefined';
}

exports.getNewCoffee=(item)=> {
    var obj = {};
    obj.id=item.id;
    obj.name=item.name;
    obj.category=item.category.title;
    obj.category_id=item.category.id;
    obj.description=item.description;
    obj.price=item.price;
    obj.image=item.image;
     return obj;
  }

// Check File Type
const checkFileType=(file,cb)=>{
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null,true);
    }else {
        cb('Error: Images Only!');
    }
}

// Set Storage Engine
const storage = multer.diskStorage({
    destination: './public/images/coffee',
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

// Image upload
exports.upload = multer({
    storage: storage,
    limits: {fileSize: 1000000},
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb);
    }
}).single('myImage')


