"use strict";
const sequelize = require('sequelize');
const models = require('../models/index');

const db = require('../db/db.config');

exports.get = (model, query, isSingle = false, isDeleted = false) => {

    return new Promise((resolve, reject) => {
        if (isSingle) {
          models[model].findOne(query)
                .then(success => resolve(success))
                .catch(error => {
                    console.log('ERROR repo get : ', error);
                    reject(error);
                })
        } else {

            models[model].findAll(query)
                .then(success => resolve(success))
                .catch(error => {
                    console.log('ERROR repo get : ', error);
                    reject(error);
                })
        }
    });
};
exports.getAndCount = (model, query, isSingle = false, isDeleted = false) => {
    return new Promise((resolve, reject) => {
        models[model].findAndCountAll(query)
            .then(success => resolve(success))
            .catch(error => {
                console.log('ERROR repo get : ', error);
                reject(error);
            })
    });
};
exports.save = (model, object) => {
    return new Promise((resolve, reject) => {
        models[model].create(object).then(success => resolve(success))
            .catch(error => {
                console.log('ERROR repo save : ', error);
                reject(error);
            })
    })
};
exports.register = (model, object) => {
    return new Promise((resolve, reject) => {
        object.password = generateHash(object.password);
        models[model].create(object).then(success => resolve(success))
            .catch(error => {
                console.log('ERROR repo save : ', error);
                reject(error);
            })
    })
};
exports.update = (model, query, updateData) => {
    return new Promise((resolve, reject) => {
        models[model].update(updateData, query).then(success => resolve(success))
            .catch(error => {
                console.log('ERROR repo save : ', error);
                reject(error);
            })
    });
};
exports.delete = (model, query) => {
    return new Promise((resolve, reject) => {
        models[model].destroy(query).then(success => resolve(success))
            .catch(error => {
                console.log('ERROR repo save : ', error);
                reject(error);
            })
    });
};
exports.rawQuery = (query) => {
    return new Promise((res,rej)=> db.query(query,)
        .then(data => res(data)).catch(e => rej(e)))
};
exports.Op = db.Op;
exports.sequelize = sequelize;