
const config = require('../config/dbconfiguration');

const Sequelize = require('sequelize');

const db = new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASS, {
    host: config.DB_URI,
    dialect: config.DB_DIALECT,
    port:'3306',


    // logging: false,
    pool: {
        max: process.env.POOL_MAX,
        min: process.env.POOL_MIN,
        acquire: process.env.POOL_ACQUIRE,
        idle: process.env.POOL_IDLE
    },

    define: {
        timestamps: false
    },
    dialectOptions: {
        // useUTC: false, //for reading from database
        dateStrings: true,
        typeCast: true
    },
    timezone: '+05:00' //for writing to database ( it will prevent date modification in query)
});



db.authenticate()
    .then(() => console.log('DB Connection has been established successfully.'),(err) => console.log(err.name))
    .catch(err => console.error('Unable to connect to the database:', err));
module.exports = db;






