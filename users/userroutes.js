const usercontroller = require('./usercontroller');
const helper = require('../utils/helper');
const passport = require('passport');

require('../auth/auth');

module.exports = (app) => {

    app.get("/api/users", passport.authenticate('jwt', {session: false}), usercontroller.getUsers);
    app.get("/api/users/:id", usercontroller.getUser);
    app.post("/api/user/register",passport.authenticate('signup', {session: false}), usercontroller.saveUser);
    app.put("/api/users/:id", usercontroller.updateUser);
    app.delete("/api/users/:id", usercontroller.deleteUser);
    app.post("/api/user/login", usercontroller.loginUser);
}
