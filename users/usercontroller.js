const db = require('../db/dbcore');
const helper = require('../utils/helper');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');

exports.loginUser = async (req, res, next) => {
    passport.authenticate('login', async (err, user, info) => {
        try {
            if (err || !user) {
                const error = {message: " User not found ! Login Failed ", status: false};
                return res.json(error);
            }
            req.login(user, {session: false}, async (error) => {
                if (error) return next(error)
                //We don't want to store the sensitive information such as the
                //user password in the token so we pick only the email and id
                const body = {id: user.id, email: user.email};
                //Sign the JWT token and populate the payload with the user email and id
                const token = jwt.sign({user: body}, 'top_secret');
                //Send back the token to the user
                return res.json({token, body});
            });
        } catch (error) {
            return next(error);
        }
    })(req, res, next);
}

exports.getUsers = async function (req, res) {
    let query = {};
    query.attributes = ['id', 'email', 'password', 'username'];
//    query.include = [{model:models.category, as:'category'}
//    ,{model:models.user, as:'users', through: {attributes: ["total","quantity"]}}
// ];

    let result = await db.get('user', query, false);


    if (result !== null) {
        res.json(result);
    } else
        res.json({status: "No Data available."});
};

exports.getUser = async function (req, res) {

    let query = {};
    query.attributes = ['id', 'email', 'password', 'username'];
    // query.include = [
    //     {model:models.category, as:'category'},
    //     // {model:models.user, as:'users'}
    // ];
    query.where = {id: req.params.id};

    let result = await db.get('user', query, true);

    (result !== null) ?
        res.json(result) :
        res.json({status: "No Data available"});
}

exports.saveUser = async function (req, res) {
    res.json({message: 'Signup successful', user: req.user, status: true})
};

exports.updateUser = async function (req, res) {
    let query = {}
    query.attributes = ['email', 'password', 'username'];
    // query.include = [{model:models.category, as:'category'}];
    query.where = {id: req.params.id};

    let newItem = {
        email: req.body.email,
        password: req.body.password,
        username: req.body.username
    }

    let result = await db.update('user', query, newItem);

    (result || result.length > 0) ?
        res.json({"status": "updated successfully"}) :
        res.json({status: "No Data available"});
}

exports.deleteUser = async (req, res) => {
    let query = {};
    query.attributes = ['email', 'password', 'username'];
    query.where = {id: req.params.id};

    let result = await db.delete('user', query);

    (result != null) ?
        res.json(result) :
        res.json({status: "Item not deleted"});
}
