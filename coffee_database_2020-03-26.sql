-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.14 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table coffee_database.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table coffee_database.categories: ~3 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(2, 'Al-Mond', '2020-03-28 10:17:28', '2020-03-28 10:17:28'),
	(3, 'Al-ChownMan', '2020-03-28 10:17:54', '2020-03-28 10:17:54'),
	(4, 'Al-Khaleej', '2020-03-28 10:18:02', '2020-03-28 10:18:02');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table coffee_database.coffees
CREATE TABLE IF NOT EXISTS `coffees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT '',
  `category_id` int(11) NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `description` text,
  `image` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `category` (`category_id`),
  CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table coffee_database.coffees: ~1 rows (approximately)
/*!40000 ALTER TABLE `coffees` DISABLE KEYS */;
INSERT INTO `coffees` (`id`, `name`, `category_id`, `price`, `description`, `image`) VALUES
	(1, 'azfarCoff', 3, 12132.00, 'czxbvbvvcxvcxv', '/images/coffee/myImage-1586269397104.jpg');
/*!40000 ALTER TABLE `coffees` ENABLE KEYS */;

-- Dumping structure for table coffee_database.customer__customers
CREATE TABLE IF NOT EXISTS `customer__customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) NOT NULL DEFAULT '',
  `last_name` varchar(15) NOT NULL DEFAULT '',
  `email` varchar(30) NOT NULL DEFAULT '',
  `contact` varchar(30) NOT NULL DEFAULT '',
  `status` int(1) NOT NULL,
  `image` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table coffee_database.customer__customers: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer__customers` DISABLE KEYS */;
INSERT INTO `customer__customers` (`id`, `first_name`, `last_name`, `email`, `contact`, `status`, `image`) VALUES
	(1, 'azfar', 'pervaiz', 'azfar@gmail.com', '03331276435', 1, ''),
	(2, 'Daniyal', 'Moeen', 'DaniyalMoeen@gmail.com', '033312432132', 1, '');
/*!40000 ALTER TABLE `customer__customers` ENABLE KEYS */;

-- Dumping structure for table coffee_database.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '0',
  `username` varchar(30) DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password` (`password`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table coffee_database.users: 3 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `username`, `created_at`, `updated_at`) VALUES
	(14, 'minhal@gmail213.com', '$2b$10$FujBsFBzGTNIRULiIXb/POMXzduZbhVtfdbB6kN0.MgX8LaNlrqia', NULL, '2020-04-11 19:51:40', '2020-04-11 19:51:40'),
	(16, 'azfar@gmail.com', '$2b$10$FUB0njGJozg0oeqiStfpn.P2xpVubq7u5ZwXiQWnuhDA4EHrl85YW', NULL, '2020-04-11 21:35:55', '2020-04-11 21:35:55'),
	(17, 'zumer@hotmail.com', '$2b$10$39.X5c5329H/Kj5.I0oFzOYAO6/dwLmDB8d75plXEBBwMt/c6sMp6', NULL, '2020-04-14 20:02:47', '2020-04-14 20:02:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table coffee_database.user_coffees
CREATE TABLE IF NOT EXISTS `user_coffees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `coffee_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` double(10,2) NOT NULL,
  `quantity` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table coffee_database.user_coffees: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_coffees` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_coffees` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
