'use strict';
const Sequelize = require('sequelize')
module.exports = (sequelize) => {
    const category = sequelize.define('categories', {
        title: Sequelize.STRING,
    }, {});
    category.associate = function (models) {
        category.hasMany(models.coffee , {foreignKey:'category_id',as : 'coffees' })
    };
    return category;
};