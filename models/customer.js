'use strict';
const Sequelize = require('sequelize')
module.exports = (sequelize) => {
    const customer = sequelize.define('customer__customers', {
        first_name: Sequelize.STRING,
        last_name: Sequelize.STRING,
        email: Sequelize.STRING,
        contact: Sequelize.STRING,
        status: Sequelize.INTEGER,
        image: Sequelize.STRING
    }, {});

    // user.associate = function (models) {
    //      user.belongsToMany(models.coffee,{through : 'user_coffees', as : 'coffees', foreignKey:'user_id'});
    //  };

    return customer;
};