'use strict';
const Sequelize = require('sequelize')
module.exports = (sequelize) => {
    const coffee = sequelize.define('coffees', {
        name: Sequelize.STRING,
        category_id: Sequelize.INTEGER,
        price: Sequelize.DOUBLE,
        description: Sequelize.TEXT,
        image: Sequelize.STRING
    }, {});
    coffee.associate = function (models) {
        coffee.belongsTo(models.category,{foreignKey:'category_id' , as: 'category'});
        // coffee.belongsToMany(models.user,{through : 'user_coffees', foreignKey:'coffee_id' , as : 'users'});
    };
    return coffee;
};