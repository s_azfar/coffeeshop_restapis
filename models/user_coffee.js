'use strict';
const Sequelize = require('sequelize')
module.exports = (sequelize) => {
    const user_coffee = sequelize.define('user_coffees', {
        user_id: Sequelize.INTEGER,
        coffee_id: Sequelize.INTEGER,
        total:Sequelize.DOUBLE,
        quantity:Sequelize.INTEGER
    }, {});

    user_coffee.associate=function(models){
        user_coffee.belongsTo(models.coffee,{foreignKey:"coffee_id"});
        // user_coffee.belongsTo(models.user,{foreignKey:"user_id"});
    }
    
    
    return user_coffee;
};