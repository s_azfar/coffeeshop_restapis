'use strict';
const Sequelize = require('sequelize');
const db = require('../db/db.config');
const coffee = require('./coffee')(db);
const category = require('./category')(db);
const customer = require ('./customer')(db);
const user_coffee = require('./user_coffee')(db);
const user = require('./user')(db);

let dbModels = {
    coffee,category,customer,user_coffee,user
};

Object.keys(dbModels).forEach(modelName => {
    if (dbModels[modelName].associate) {
        dbModels[modelName].associate(dbModels);
    }
});
//
// db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = dbModels;
