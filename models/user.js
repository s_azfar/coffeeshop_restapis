'use strict';
const Sequelize = require('sequelize')
module.exports = (sequelize) => {
    const user = sequelize.define('users', {
        email: Sequelize.STRING,
        password: Sequelize.STRING,
        username: Sequelize.STRING,
    }, {});

    // user.associate = function (models) {
    //      user.belongsToMany(models.coffee,{through : 'user_coffees', as : 'coffees', foreignKey:'user_id'});
    //  };

    return user;
};