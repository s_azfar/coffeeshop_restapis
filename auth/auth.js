const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const UserModel = require('../models/index');
const db = require('../db/dbcore');
const bcrypt = require('bcrypt');

//Create a passport middleware to handle user registration

passport.use('signup', new localStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    //Save the information provided by the user to the the database

    async (email, password, done) => {

        try {
            const hashedPassword = await bcrypt.hash(password, 10)

            let query = {
                email: email,
                password: hashedPassword,
                username: null,

            };


            const user = await db.save('user', query);


            //Send the user information to the next middleware
            return done(null, user);
        } catch (error) {
            done(error);
        }
    }));

//Create a passport middleware to handle User login

passport.use('login', new localStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    async (email, password, done) => {
    try {
        //Find the user associated with the email provided by the user

        let query = {};
        query.attributes = ['id', 'email', 'password', 'username'];
        // query.include = [
        //     {model:models.category, as:'category'},
        //     // {model:models.user, as:'users'}
        // ];
        query.where = {email: email};

        const user = await db.get('user', query, true);

        if (!user) {
            //If the user isn't found in the database, return a message
            return done(null, false, {message: 'User not found'});
        }
        //Validate password and make sure it matches with the corresponding hash stored in the database
        //If the passwords match, it returns a value of true.

        try {
            if (await bcrypt.compare(password, user.password)) {
                return done(null, user, {message: 'Logged in Successfully'});

            } else {
                return done(null, false, {message: 'Password Incorrect !'})
            }
        } catch (e) {
            return done(e)
        }

        //Send the user information to the next middleware
    } catch (error) {
        return done(error);
    }
}));

const JWTstrategy = require('passport-jwt').Strategy;
//We use this to extract the JWT sent by the user
const ExtractJWT = require('passport-jwt').ExtractJwt;

//This verifies that the token sent by the user is valid
passport.use(new JWTstrategy({
    //secret we used to sign our JWT
    secretOrKey: 'top_secret',
    //we expect the user to send the token as a query parameter with the name 'secret_token'
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken('secret_token')
}, async (token, done) => {
    try {
        //Pass the user details to the next middleware
        return done(null, token.user);
    } catch (error) {
        done(error);
    }
}));
