const categoriescontroller = require ('./categoriescontroller');

module.exports=(app)=>{
// Categories for dropdown
app.get("/api/categories/dropdown",categoriescontroller.getCategoryList);    
app.get("/api/categories",categoriescontroller.getCategories);
app.get("/api/categories/:id",categoriescontroller.getSingleCategory);
app.post("/api/categories",categoriescontroller.saveCategory );    
app.put("/api/categories/:id", categoriescontroller.updateCategory);
app.delete("/api/categories/:id",categoriescontroller.deleteCategory);
}
