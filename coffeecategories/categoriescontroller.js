const models= require('../models/index');
const db= require('../db/dbcore');
const helper=require('../utils/helper');

exports.getCategories= async (req,res)=>{

   let query={};
   query.include = [{model:models.coffee, as:'coffees'}];
   query.attributes=['id','title'];

    let result = await db.get('category',query,false);
   (result!==null) ?
    res.json(result) :
    res.json({status:"No Data available."});

};

exports.getCategoryList= async (req,res)=>{

    let query={};
    query.attributes=['id','title'];
 
     let result = await db.get('category',query,false);
    (result!==null) ?
     res.json(result) :
     res.json({status:"No Data available."});
 
 };

exports.getSingleCategory = async (req,res) => {

    let query = {};
    query.attributes=['id','title'];
    query.include = [{model:models.coffee, as:'coffees'}];
    query.where={id:req.params.id};

    let result = await db.get ('category',query,true);

    (result!==null) ?
    res.json(result) :
    res.json({status:"No Data available"});
}

exports.saveCategory = async (req,res) => {
        let query = {
        title:req.body.title
    };

    // return res.json(req.body);

    let result = await db.save ('category',query);

    (result!==null) ?
    res.json(result) :
    res.json({status:"No Data available"});
    
}

exports.updateCategory = async (req,res) => {
    let query = {}
    query.attributes=['title'];
    query.include = [{model:models.coffee, as:'coffee'}];
    query.where = {id:req.params.id};
    
    let newItem = {
        title:req.body.title
     }
        
     let result = await db.update ('category',query,newItem);

     (result!==null) ?
     res.json(result) :
     res.json({status:"No Data available"});
}

exports.deleteCategory = async (req,res) => {
    let query = {};
    query.attributes=['title'];
    query.where = {id:req.params.id};

    let result = await db.delete('category',query);

    (result!=null) ? 
    res.json(result) : 
    res.json({status : "Item not deleted"});
}