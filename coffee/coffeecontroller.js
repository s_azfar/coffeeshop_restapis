const models= require('../models/index');
const db= require('../db/dbcore');
const helper=require('../utils/helper');

exports.getAllCoffees= async function(req,res){
   let query={};
   query.attributes=['id','name','category_id','description','price','image'];
   query.include = [{model:models.category, as:'category'}
//    ,{model:models.user, as:'users', through: {attributes: ["total","quantity"]}}
];

    let result = await db.get('coffee',query,false);


    if (result!==null) {
        res.json(result.map(helper.getNewCoffee));
    }
    else
     res.json({status:"No Data available."});
};

exports.getSingleCoffee = async function (req,res){

    let query = {};
    query.attributes=['id','name','category_id','description','price','image'];
    query.include = [
        {model:models.category, as:'category'},
        // {model:models.user, as:'users'}
    ];
    query.where={id:req.params.id};

    let result = await db.get ('coffee',query,true);

    (result!==null) ?
    res.json(result) :
    res.json({status:"No Data available"});
}

exports.saveCoffee = async function (req,res){
        let query = {
        name:req.body.name,
        category_id:req.body.category_id,
        price:req.body.price,
        description:req.body.description,
            // String interpolation
        image: `/images/coffee/${req.file.filename}`
    };


    let result = await db.save ('coffee',query);

    (result!==null) ?
    res.json(result) :
    res.json({status:"No Data available"});


}

exports.updateCoffee = async function(req,res){
    let query = {}
    query.attributes=['name','category_id','description','price','image'];
    query.include = [{model:models.category, as:'category'}];
    query.where = {id:req.params.id};

    let newItem = {
        name:req.body.name,
        category_id:req.body.category_id,
        price:req.body.price,
        description:req.body.description,
        image:req.body.image
     }

     let result = await db.update ('coffee',query,newItem);

     (result||result.length>0) ?
     res.json({"status":"updated successfully"}) :
     res.json({status:"No Data available"});
}

exports.deleteCoffee = async (req,res) => {
    let query = {};
    query.attributes=['name','category_id','description','price','image'];
    query.where = {id:req.params.id};

    let result = await db.delete('coffee',query);

    (result!=null) ?
    res.json(result) :
    res.json({status : "Item not deleted"});
}
