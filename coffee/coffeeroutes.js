const passport = require('passport');

const coffeecontroller = require('./coffeecontroller');
const helper = require('../utils/helper');

module.exports = (app) => {

    app.get("/api/coffees", coffeecontroller.getAllCoffees);
    app.get("/api/coffees/:id", coffeecontroller.getSingleCoffee);
    app.post("/api/coffees", helper.upload, coffeecontroller.saveCoffee);
    app.put("/api/coffees/:id", coffeecontroller.updateCoffee);
    app.delete("/api/coffees/:id", coffeecontroller.deleteCoffee);


}
